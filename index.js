const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");
const AWS = require("aws-sdk");
const StorageBase = require("ghost-storage-base");
const readFileAsync = Promise.promisify(fs.readFile);

class S3Store extends StorageBase {
    // Main methods

    constructor(config) {
        super();

        // Validate config
        this.validateConfig(config);

        // Set opts and create s3 client
        this.options = config;
        this.s3 = new AWS.S3({
            bucket: this.options.bucket,
            region: this.options.region
        });
    }

    async save(image, targetDir) {
        targetDir = targetDir || this.getTargetDir();

        const targetFilename = this.getTargetName(image, targetDir);
        const buffer = await readFileAsync(image.path);
        const daysToSecs = 86400;
        const maxAgeDays = 365;
        const maxAgeSecs = maxAgeDays * daysToSecs;

        const params = {
            ACL: "public-read",
            Bucket: this.options.bucket,
            Key: targetFilename,
            Body: buffer,
            ContentType: image.type,
            CacheControl: `max-age=${maxAgeSecs}`
        };

        // Upload
        await this.s3.putObject(params).promise();

        // Build reponse
        const awsPath = this.getAwsPath(this.options);
        const fileUrl = awsPath + targetFilename;

        // TODO: Promise.resolve is redundant
        return Promise.resolve(fileUrl);
    }

    serve() {
        return (req, res, next) => {
            next();
        };
    }

    // Not implemented methods

    saveRaw() {
        return Promise.reject("not implemented");
    }

    exists() {
        return Promise.reject("not implemented");
    }

    delete() {
        return Promise.reject("not implemented");
    }

    read() {
        return Promise.reject("not implemented");
    }

    // Helpers

    validateConfig(opts) {
        if (!opts) {
            throw new Error("ghost-s3 config is missing");
        }

        if (!opts.region) {
            throw new Error("ghost-s3 config is missing `region` property");
        }

        if (!opts.bucket) {
            throw new Error("ghost-s3 config is missing `bucket` property");
        }
    }

    getTargetName(image, targetDir) {
        const ext = path.extname(image.name);
        const name = path.basename(image.name, ext).replace(/\W/g, "_");

        return `${targetDir}${name}-${Date.now()}${ext}`;
    }

    getAwsPath(opts) {
        const awsRegionSub =
            opts.region === "us-east-1" ? "s3" : `s3-${opts.region}`;
        const awsPath = opts.assetHost
            ? opts.assetHost
            : `https://${awsRegionSub}.amazonaws.com/${opts.bucket}/`;
        return awsPath;
    }
}

module.exports = S3Store;
